import React, {useEffect, useRef, useState} from 'react';
import {
  Alert,
  Button,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
} from 'react-native';
import * as LocalAuthentication from 'expo-local-authentication';
import ReactNativeBiometrics, {BiometryTypes} from 'react-native-biometrics';

export default function App() {
  const [isBiometricSupported, setIsBiometricSupported] = useState(false);
  const rnBiometrics = new ReactNativeBiometrics();
  // const { biometryType } = await rnBiometrics.isSensorAvailable()

  // For face detection or fingerprint scan
  useEffect(() => {
    (async () => {
      const compatible = await LocalAuthentication.hasHardwareAsync();
      setIsBiometricSupported(compatible);
    })();
  });

  // useEffect(() => {
  //   console.log('HELLO');
  //   async () => {
  //     const sensorAvailable = await rnBiometrics.isSensorAvailable();
  //     console.log('Sensor info: ', sensorAvailable);

  //     sensorAvailable.then(resultObject => {
  //       console.log('Sensor info: ', resultObject);
  //       const {available, biometryType} = resultObject;
  //       setIsBiometricSupported(available);

  //       if (available) {
  //         console.log('Biometry ', biometryType, ' is supported');
  //       } else {
  //         console.log('Biometrics not supported');
  //       }
  //     });
  //   };
  // });

  const fallBackToDefaultAuth = () => {
    console.log('fall back to password authentication');
  };

  const alertComponent = (title, mess, btnTxt, btnFunc) => {
    return Alert.alert(title, mess, [
      {
        text: btnTxt,
        onPress: btnFunc,
      },
    ]);
  };

  const TwoButtonAlert = () => {
    Alert.alert('Welcome to App', 'Subscribe Now', [
      {
        text: 'Back',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {
        text: 'OK',
        onPress: () => console.log('Ok Pressed'),
      },
    ]);
  };

  const handleRNBioAuth = () => {
    rnBiometrics
      .simplePrompt({promptMessage: 'Confirm fingerprint'})
      .then(resultObject => {
        const {success} = resultObject;

        if (success) {
          console.log('successful biometrics provided');
        } else {
          console.log('user cancelled biometric prompt');
        }
      })
      .catch(() => {
        console.log('biometrics failed');
      });
  };

  const handleBiometricAuth = async () => {
    // check if hardware supports biometric
    try {
      const isBiometricAvailable = await LocalAuthentication.hasHardwareAsync();

      // fall back to password
      if (!isBiometricSupported) {
        return alertComponent(
          'Please Enter Your Password',
          'Biometric Auth not Supported',
          'Ok',
          () => fallBackToDefaultAuth(),
        );
      }
      // check biometric types available
      let supportedBiometrics;
      if (isBiometricAvailable) {
        supportedBiometrics =
          await LocalAuthentication.supportedAuthenticationTypesAsync();
      }
      // check biometrics are saved locally
      const savedBiometrics = await LocalAuthentication.isEnrolledAsync();
      if (!savedBiometrics) {
        return alertComponent(
          'Biometric record not found',
          'Please Login With Password',
          'Ok',
          () => fallBackToDefaultAuth(),
        );
      }
      // authenticate with biometric
      const biometricAuth = await LocalAuthentication.authenticateAsync({
        promptMessage: 'Login With Biometrics',
        cancelLabel: 'cancel',
        fallbackLabel: '',
        disableDeviceFallback: true,
      });

      // Log the user in on success
      if (biometricAuth.success === true) {
        TwoButtonAlert();
      }
      console.log({isBiometricAvailable});
      console.log({supportedBiometrics});
      console.log({savedBiometrics});
      console.log({biometricAuth});
    } catch (err) {
      console.log('Error: ', err);

      return alertComponent(
        'Please Enter Your Password',
        'Biometric Auth not Supported',
        'Ok',
        () => fallBackToDefaultAuth(),
      );
    }
  };

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'gray',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });

  return (
    <View style={styles.container}>
      <Text>
        {isBiometricSupported
          ? 'Your Device Is Compatible With Biometrics'
          : 'Face or Fingerprint scanner is NOT available on this device!'}
      </Text>
      <TouchableHighlight
        style={{
          heigh: 60,
          marginTop: 200,
        }}>
        <Button
          title="Login With Biometrics"
          color="black"
          onPress={handleBiometricAuth}
        />
      </TouchableHighlight>
      <StatusBar style="auto" />
    </View>
  );
}
